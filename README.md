# Markdown转HTML

#### 安装教程

```bash
composer require dgwht/parsedown
```



#### 使用说明

> 直接使用

```php
use dgwht/Parsedown;

$Parsedown = new Parsedown();

echo $Parsedown->text('Hello _Parsedown_!');
```



